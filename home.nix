{ config, lib, pkgs, ... }:
let
  username = "teo";
  shellAliases = { };
in {
  imports = [ ./user/emacs.nix ./user/neovim.nix ];

  home = {
    inherit username;
    homeDirectory = "/home/${username}";
    packages = with pkgs; [
      tmux
      topgrade
      htop
      glances # nicer htop
      fortune
      fd
      pciutils # for lspci
      onefetch # show git repo summary
      neofetch # show system summary
      ranger # file manager with kitty support
      shellcheck # shell linter
      nixfmt
      dtach # detach a process and reattach later
      jq
      yq
      darcs # vcs based on a theory of patches
      nix-output-monitor # pretty print nix-build/etc

      # tree and friends
      tree
      tre-command # tree but uses .gitignore and allows yout to easily opens files
      niv
      figlet # unicode text generator
      browsh # a tui browser
      glab # gitlab cli
      gitAndTools.git-absorb # auto fixup (required by magit)
      gitAndTools.gitui # git tui
      perl532Packages.GitAutofixup # auto fixup (required by magit)
      cached-nix-shell
      nix-tree # display a derivations inputs
      procs # like ps
      nix-top # tells you what's building
      #arion # docker-compose but nixier
      # nix-du # like du but for nix 
      graphviz # dot and friends
      xdot # dot graph gui

      bottom
      iftop # bandwidth usage monitor

      cachix

      pass
    ];
  };

  home.sessionVariables = {
    EDITOR = "vim";
  }; # we like emacs but for small tasks vim is quicker

  programs.emacs.enable = true;
  programs.neovim.enable = true;

  programs.git = {
    enable = true;
    userName = "Teo Camarasu";
    userEmail = lib.mkDefault "teofilcamarasu@gmail.com";
    delta.enable = true;
    aliases = {
      c = "checkout";
      cb = "checkout -b";
      p = "pull";
      pr = "pull --rebase";
      update = "! git fetch --all && git rebase origin/HEAD";
    };
    extraConfig = {
      pull = { ff = "only"; };
      push.default = "current";
      init.defaultMain = "main";
      rebase.autoStash = "true";
      rerere.enabled = "true";
      merge.conflictstyle = "diff3";
    };
  };

  programs.bash = {
    enable = true;
    profileExtra = "neofetch";
    historyControl = [ "ignoredups" ];
    inherit shellAliases;
  };

  programs.fish = {
    enable = true;
    interactiveShellInit = "neofetch";
    inherit shellAliases;
  };

  # cross shell prompt
  programs.starship = { enable = true; };

  programs.bat.enable = true; # a cat clone with wings

  programs.kitty.enable = true;

  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;

  programs.fzf.enable = true;

  programs.zoxide.enable = true;

  programs.broot = # tree but nicer
    {
      enable = true;
      enableBashIntegration = true;
      enableFishIntegration = true;
      # TODO: try out modal mode
    };

  programs.exa.enable = true; # better ls

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
