{ config, lib, pkgs, ... }: {
  options.gnome.enable = lib.mkEnableOption "enable xserver with gnome";
  config = lib.mkIf config.gnome.enable {
    services.xserver = {
      enable = true;
      desktopManager.gnome3.enable = true;
      displayManager.gdm.enable = true;
      layout = "ie";
    };

    hardware.opengl = {
      enable = true;
      driSupport32Bit = true;
    };
  };
}
