{ config, lib, ... }:
lib.mkIf config.hardware.pulseaudio.enable {
  sound.mediaKeys.enable = true;
  hardware.pulseaudio = { support32Bit = true; };
  users.users.teo.extraGroups = [ "audio" ];
}
