{
  imports = [
    ./audio.nix
    ./cachix.nix
    ./fonts.nix
    ./gitlab-runner.nix
    ./gnome.nix
    ./printing.nix
    ./time.nix
    ./touchpad.nix
    ./wsl
  ];
}
