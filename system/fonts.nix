{ pkgs, ... }: {
  environment.noXlibs = false;
  # Fonts
  fonts = {
    fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      inconsolata
      proggyfonts
      open-sans
      terminus_font_ttf
      cabin
      comic-neue
      dejavu_fonts
      league-of-moveable-type
      source-code-pro
      mononoki
      emacs-all-the-icons-fonts # for doom emacs
    ];
  };
}
