{ config, pkgs, lib, ... }: {
  config = lib.mkIf config.services.printing.enable {
    # Scanning
    hardware.sane.enable = true;
    hardware.sane.extraBackends = [ pkgs.sane-airscan ];
    users.users.teo.extraGroups = [ "scanner" ];
  };

}
