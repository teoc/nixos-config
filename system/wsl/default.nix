{ lib, pkgs, config, modulesPath, ... }:

with lib;
let
  defaultUser = "teo";
  syschdemd = import ./syschdemd.nix { inherit lib pkgs config defaultUser; };
  cfg = config.system.wsl;
in {
  options.system.wsl.enable = mkEnableOption "wsl2 workarounds";
  config = mkIf cfg.enable {
    # WSL is closer to a container than anything else
    boot.isContainer = true;

    environment.etc.hosts.enable = false;
    environment.etc."resolv.conf".enable = false;

    networking.dhcpcd.enable = false;

    environment.etc."wsl.conf".text = ''
      [network]
      generateResolvConf = false
    '';
    networking.nameservers = [ "8.8.8.8" ];

    users.users.root = {
      shell = "${syschdemd}/bin/syschdemd";
      # Otherwise WSL fails to login as root with "initgroups failed 5"
      extraGroups = [ "root" ];
    };

    security.sudo.wheelNeedsPassword = false;

    # Disable systemd units that don't make sense on WSL
    systemd.services."serial-getty@ttyS0".enable = false;
    systemd.services."serial-getty@hvc0".enable = false;
    systemd.services."getty@tty1".enable = false;
    systemd.services."autovt@".enable = false;

    systemd.services.firewall.enable = false;
    systemd.services.systemd-resolved.enable = false;
    systemd.services.systemd-udevd.enable = false;

    # Don't allow emergency mode, because we don't have a console.
    systemd.enableEmergencyMode = false;

    systemd.services.binfmt = {
      serviceConfig.type = "oneshot";
      script =
        "${pkgs.mount}/bin/mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc";
      wantedBy = [ "sysinit.target" ];
    };
  };
}
