This is from <https://github.com/Trundle/NixOS-WSL>, which is released under the Apache License 2.0.

Changes:
- turn configuration.nix into a nixos module and move out use of profile 
