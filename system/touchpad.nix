{ config, lib, ... }: {
  options.hardware.touchpad.enable = lib.mkEnableOption "touchpad/trackpoint";

  config = lib.mkIf config.hardware.touchpad.enable {
    services.xserver.libinput.enable = true; # enable touchpad

    # Configure trackpoint
    hardware.trackpoint = {
      sensitivity = 128;
      speed = 128;
    };
  };
}
