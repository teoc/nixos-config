# Nix(OS) config

layout:
  - hosts: configuration.nix for NixOS hosts
  - user: home-manager modules
  - system: NixOS modules
  - pkgs: nix package definitions
  - lib: common functions
  - doom: doom settings (symlinked to .config/doom)
  - home.nix: home-manager
  - notes: notes on nix{,os,pkgs}
