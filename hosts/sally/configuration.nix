{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ../common.nix ];

  sops.defaultSopsFile = ../../secrets/default.yaml;
  sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];

  nix.generateNixPathFromInputs = true;
  nix.generateRegistryFromInputs = true;

  nix.settings.trusted-users = [ "teo" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "sally"; # Define your hostname.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.eth0.ipv4.addresses = [{
    address = "192.168.1.11";
    prefixLength = 24;
  }];

  networking.defaultGateway = "192.168.1.1";
  networking.nameservers = [ "192.168.1.1" "8.8.8.8" ];

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    #  font = "Lat2-Terminus16";
    keyMap = "uk";
  };

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.teo = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [ wget vim docker-client ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  programs = {
    mosh.enable = true;
    tmux.enable = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    forwardX11 = true;
  };
  services.xserver = {
    enable = false;
    modules = [ pkgs.xorg.xf86videofbdev ];
    videoDrivers = [ "hyperv_fb" ];
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 80 ];
  networking.firewall.allowedUDPPorts = [ config.services.tailscale.port ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # avahi
  services.avahi.enable = true;

  services.logind.extraConfig =
    "RuntimeDirectorySize=8G"; # sometimes hyperv assigns too little here

  # Nginx
  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    virtualHosts.sally.locations = {
      "/".proxyPass = "http://localhost:8004";
      "/hledger/".proxyPass = "http://localhost:8001/";
      "/gitea/".proxyPass = "http://localhost:8002/";
      "/nextcloud/".proxyPass = "http://localhost:8003/";
      "/.well-known/carddav".proxyPass = "http://localhost:8003/remote.php/dav";
      "/.well-known/caldav".proxyPass = "http://localhost:8003/remote.php/dav";
    };
  };

  systemd.services.hledger = {
    description = "hledger";
    wantedBy = [ "multi-user.target" ];
    after = [ "networking.target" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.hledger-web}/bin/hledger-web \
          --base-url="http://sally/hledger/" \
          --host=localhost \
          --port=8001 \
          --file=/home/teo/ledger/2021.hledger
      '';
      Restart = "always";
    };
  };

  # Docker

  virtualisation.docker.enable = true;
  services.gitlab-runner.enable = true;

  virtualisation.oci-containers.containers = {
    gitea = {
      image = "gitea/gitea:latest";
      environment = {
        ROOT_URL = "http://sally/gitea/";

      };
      volumes = [
        "/mnt/d/gitea/data:/data"
        "/etc/timezone:/etc/timezone:ro"
        "/etc/localtime:/etc/localtime:ro"
      ];
      ports = [ "8002:3000" "222:22" ];
    };
    nextcloud-db = {
      image = "postgres";
      volumes = [ "/mnt/d/nextcloud-db:/var/lib/postgresql/data" ];
      environment = {
        POSTGRES_DB = "nextcloud";
        POSTGRES_USER = "nextcloud";
      };
      extraOptions = [
        "--hostname=nextcloud-db"
        "--network=nextcloud"
        "--env-file=/home/teo/nextclouddbpass"
      ];
    };
    nextcloud = {
      image = "nextcloud";
      ports = [ "8003:80" ];
      volumes = [ "/mnt/d/nextcloud:/var/www/html" ];
      dependsOn = [ "nextcloud-db" ];
      environment = {
        #SQLITE_DATABASE = "nextcloud";
        POSTGRES_HOST = "nextcloud-db";
        POSTGRES_DB = "nextcloud";
        POSTGRES_USER = "nextcloud";
        # TRUSTEDPROXIES = "192.168.1.11/nextcloud";
        OVERWRITEHOST = "sally";
        OVERWRITEWEBROOT = "/nextcloud";
      };
      extraOptions = [
        "--hostname=nextcloud"
        "--network=nextcloud"
        "--env-file=/home/teo/nextclouddbpass"
      ];
    };
    homer = {
      image = "b4bz/homer:latest";
      ports = [ "8004:8080" ];
      volumes = [ "/mnt/d/homer:/www/assets" ];
    };
  };

  systemd.services.init-nextcloud-network = {
    description = "create network for nextcloud";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];

    serviceConfig.Type = "oneshot";
    script = let dockercli = "${pkgs.docker-client}/bin/docker";
    in ''
      check=$(${dockercli} network ls | grep "nextcloud") || true
      if [ -z "$check" ]; then
        ${dockercli} network create nextcloud
        echo "nextcloud network created"
      else
        echo "nextcloud network already exists :-)"
      fi
    '';
  };

  containers.lernie = {
    # autoStart = true;
    config = { config, pkgs, ... }: {
      services.hydra = {
        enable = false;
        hydraURL = "http://sally/lernie/";
        notificationSender = "hydra@localhost";
        buildMachinesFiles = [ ];
        useSubstitutes = true;
      };
    };
  };

  services.tailscale.enable = false;

  nix.binaryCachePublicKeys =
    [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" ];
  nix.binaryCaches = [ "https://hydra.iohk.io" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}
