# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    <nixos-hardware/lenovo/thinkpad/x220>
    ./hardware-configuration.nix
    ../../system/list-modules.nix
  ];

  # Use the systemd-boot EFI boot loader.
  #boot.loader.systemd-boot.enable = true;
  boot.loader.grub.device = "nodev";
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "stuart"; # Define your hostname.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;

  hardware.pulseaudio.enable = true; # audio
  hardware.touchpad.enable = true; # touchpad
  services.printing.enable = true; # printing
  gnome.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

  nix.nixPath = [
    "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos"
    "nixos-config=/home/teo/nixos-config/hosts/stuart/configuration.nix"
    "/nix/var/nix/profiles/per-user/root/channels"
  ];

  # Packages
  environment.systemPackages = with pkgs; [
    emacs
    acpi
    wget
    google-chrome
    firefox
    git
    vim
    glxinfo
    zotero
    rclone
  ];
  nixpkgs.config.allowUnfree = true;

  boot.kernelModules = [
    "intel_pstate"
    # "phc-intel" #broken atm
  ];
  boot.extraModulePackages = with pkgs.linuxPackages;
    [
      acpi_call
      # phc-intel # broken atm
    ];

  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl
    intel-media-driver
  ];

  # Battery
  services.acpid.enable = true;
  services.tlp.enable = true;
  services.tlp.settings = {
    START_CHARGE_THRESH_BAT0 = 75;
    STOP_CHARGE_THRESH_BAT0 = 85;
  };

  # Users

  users.users.teo = {
    isNormalUser = true;
    createHome = true;
    home = "/home/teo";
    description = "Teofil Camarasu";
    extraGroups = [ "wheel" "networkmanager" "lp" ];
  };

  # Emacs
  services.emacs.enable = true;

  # Filesystems

  # Networking
  networking.networkmanager.enable = true;
  #services.dnsmasq.enable = true;
  networking.firewall.allowedTCPPorts = [ 8010 53 ]; # for chromecast
  services.avahi = {
    enable = true;
    nssmdns = true;
  };

  hardware.bluetooth.enable = true;
  #services.resolved = {
  #  enable = true;
  #};
  # internationalisaion

  # Turn off HDD after boot
  systemd.services.hdparam = {
    description = "hdparam sleep";
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${pkgs.hdparm}/bin/hdparm -q -S 1 -y /dev/sda";
    };
    wantedBy = [ "multi-user.target" ];
  };
}

