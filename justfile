list:
  just --list
switch *FLAGS: 
  home-manager switch {{FLAGS}}
update *FLAGS: 
  niv update {{FLAGS}}
  just switch
  just nixos switch
format *FLAGS:
  ./format {{FLAGS}}
nixos *FLAGS:
  sudo -E nixos-rebuild {{FLAGS}}
