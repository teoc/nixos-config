{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils-plus.url = "github:gytis-ivaskevicius/flake-utils-plus";
    flake-utils-plus.inputs.nixpkgs.follows = "nixpkgs";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    emacs-overlay.inputs.nixpkgs.follows = "nixpkgs";
    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager, flake-utils-plus, emacs-overlay
    , sops-nix }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages."${system}";
      hmConfiguration = { extraModules ? [ ] }:
        home-manager.lib.homeManagerConfiguration {
          system = "x86_64-linux";
          homeDirectory = "/home/teo";
          username = "teo";
          stateVersion = "21.03";
          configuration.imports =
            [ { nixpkgs.overlays = [ emacs-overlay.overlay ]; } ./home.nix ]
            ++ extraModules;
        };
    in flake-utils-plus.lib.mkFlake {

      inherit self inputs;

      channelsConfig = { allowUnfree = true; };

      hosts.sally.modules =
        [ ./hosts/sally/configuration.nix sops-nix.nixosModules.sops ];

      lib = { inherit hmConfiguration; };

      homeConfigurations = { 
        teo = hmConfiguration { }; 
        work = hmConfiguration { extraModules = [ ./workHome.nix ];};
      };

      checks."${system}".format = pkgs.stdenvNoCC.mkDerivation {
        name = "format-sources";
        src = ./.;
        buildInputs = [ pkgs.findutils pkgs.nixfmt ];
        buildPhase = "./format -c; touch $out";
        dontInstall = true;
      };

      devShell."${system}" = pkgs.mkShell {
        buildInputs = [
          pkgs.just
          pkgs.nixfmt
          pkgs.findutils
          home-manager.packages."${system}".home-manager
          pkgs.age
          pkgs.sops
        ];
      };
    };
}
