{ config, pkgs, lib, ... }: {
  programs.git.userEmail = "teo.camrasu@tracsis.com";
  programs.bash.enable = lib.mkForce false;
  xdg.configFile."nix/nix.conf".text = ''
    substituters = https://cache.nixos.org https://mpickering.cachix.org https://nix-community.cachix.org  https://hydra.iohk.io
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= mpickering.cachix.org-1:COxPsDJqqrggZgvKG6JeH9baHPue8/pcpYkmcBPUbeg= nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs= hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=
    auto-optimise-store = true
    keep-derivations = true
    keep-outputs = true
    cores = 8
    experimental-features = nix-command flakes ca-derivations
  '';
}
