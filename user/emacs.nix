{ config, lib, pkgs, ... }:
lib.mkIf config.programs.emacs.enable {

  programs.emacs = {
    # enable = true;
    package = pkgs.emacsGcc;
  };

  home.packages = with pkgs; [
    # most of these come from hlissner's dotfiles
    binutils # for native comp

    # we should have these anyway but let's just be sure
    git
    ripgrep
    gnutls
    fd
    imagemagick
    zstd

    (aspellWithDicts (ds: with ds; [ en en-computers en-science ]))

    # :checkers grammar
    languagetool
    # :tools editorconfig
    editorconfig-core-c
    # :tools lookup ; org roam
    sqlite
    #:tools pass
    pass
  ];

  home.sessionPath =
    [ "${config.xdg.configHome}/emacs/bin" ]; # for doom command line

  xdg.configFile.doom.source = config.lib.file.mkOutOfStoreSymlink ../doom;
}
